const elementsToTranslate = document.querySelectorAll('[data-text]');
function getBrowserLanguage() {
  return navigator.language || navigator.userLanguage;
}
function translateElements(language) {
  elementsToTranslate.forEach(element => {
    const text = element.getAttribute(`data-text-${language}`);
    if (text) {
      element.textContent = text;
    } else {
      const defaultText = element.getAttribute('data-text');
      if (defaultText) {
        element.textContent = defaultText;
      }
    }
  });
}
const browserLanguage = getBrowserLanguage().toLowerCase();
const languageCode = browserLanguage.split('-')[0];
const supportedLanguages = ['de', 'cn', 'en', 'fr', 'ja', 'ko', 'ru'];
if (supportedLanguages.includes(languageCode)) {
  translateElements(languageCode);
} else {
  translateElements('ru');
}
